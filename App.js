import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/components/HomeScreen';
import TabStack from './src/components/TabStack';
import ClubStack from './src/components/club/ClubStack';
import ServiceStack from './src/components/service/ServiceStack';
import Browser from './src/components/Browser';
import Colors from './src/res/colors';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: Colors.white,
          },
          headerTintColor: Colors.black,
        }}>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="Tabs" component={TabStack} />
        <Stack.Screen name="Club" component={ClubStack} />
        <Stack.Screen name="Service" component={ServiceStack} />
        <Stack.Screen name="Browser" component={Browser} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
