const Colors = {
  green: '#b2cb32',
  grey: '#363636',
  ligthGrey: '#9b9b9b',
  white: '#fff',
  black: '#000',
};

export default Colors;
