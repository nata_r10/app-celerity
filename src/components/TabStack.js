import React from 'react';
import {StyleSheet, Image, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Colors from 'celerity/src/res/colors';
import ClubStack from 'celerity/src/components/club/ClubStack';
import PagosStack from 'celerity/src/components/pagos/PagosStack';
import AtencionStack from 'celerity/src/components/atencion/AtencionStack';

const Tabs = createBottomTabNavigator();

const TabStack = (props) => {
  return (
    <Tabs.Navigator
      initialRouteName={props.route.params.tab}
      tabBarOptions={{
        tintColor: Colors.gray,
        style: {
          backgroundColor: Colors.white,
        },
      }}>
      <Tabs.Screen
        name="Pagos"
        component={PagosStack}
        options={{
          tabBarIcon: ({size, color}) => (
            <Image
              style={{tintColor: color, width: size, height: size}}
              source={require('celerity/src/assets/bottom-tabs/card.png')}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Call me"
        component={ClubStack}
        options={{
          tabBarIcon: ({size, color}) => (
            <Image
              style={{tintColor: color, width: size, height: size}}
              source={require('celerity/src/assets/bottom-tabs/callme.png')}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Atencion"
        component={AtencionStack}
        options={{
          tabBarIcon: ({size, color}) => (
            <Image
              style={{tintColor: color, width: size, height: size}}
              source={require('celerity/src/assets/bottom-tabs/messages.png')}
            />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default TabStack;
