import React from 'react';
import {SafeAreaView, StyleSheet, Image, View, ScrollView} from 'react-native';
import HeaderLogo from 'celerity/src/components/Header/HeaderLogo';
import HomeButton from 'celerity/src/components/Buttons/HomeButton';

const HomeScreen = (props) => {
  const navigate = (component, tab) => {
    props.navigation.navigate(component, {tab: tab});
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <HeaderLogo />
        <View style={styles.buttons}>
          <HomeButton
            image={require('celerity/src/assets/buttons/wifi.png')}
            text="Solicita tu Plan"
            navigate={() => navigate('Tabs', 'Pagos')}
          />
          <HomeButton
            image={require('celerity/src/assets/buttons/club.png')}
            text="Mi Club Celerity"
            navigate={() => navigate('Club', '')}
          />
        </View>
        <View style={styles.buttons}>
          <HomeButton
            image={require('celerity/src/assets/buttons/atention.png')}
            text="Atención al Cliente"
            navigate={() => navigate('Tabs', 'Atencion')}
          />
          <HomeButton
            image={require('celerity/src/assets/buttons/card.png')}
            text="Pagos y Facturas"
            navigate={() => navigate('Tabs', 'Pagos')}
          />
        </View>
        <View style={styles.buttons}>
          <HomeButton
            image={require('celerity/src/assets/buttons/person.png')}
            text="Servicio Técnico"
            navigate={() => navigate('Service', '')}
          />
        </View>
        <View>
          <Image
            style={styles.footer}
            source={require('celerity/src/assets/people_working.jpg')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    backgroundColor: '#fff',
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default HomeScreen;
