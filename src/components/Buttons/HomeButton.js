import React from 'react';
import {StyleSheet, Image, Text, Pressable} from 'react-native';
import Colors from '../../res/colors';

const HomeButton = ({image, text, navigate}) => {
  return (
    <Pressable onPress={navigate} style={styles.container}>
      {image && <Image style={styles.icon} source={image} />}
      <Text style={styles.text}>{text}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 108,
    height: 100,
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 15,
    backgroundColor: Colors.white,
    paddingVertical: 10,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 7,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  icon: {
    width: 50,
    height: 36,
    marginBottom: 10,
  },
  text: {
    textAlign: 'center',
    fontSize: 14,
  },
});

export default HomeButton;
