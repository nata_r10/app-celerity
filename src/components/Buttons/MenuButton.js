import React from 'react';
import {StyleSheet, Image, Text, Pressable} from 'react-native';
import Colors from '../../res/colors';

const MenuButton = ({text, navigate}) => {
  return (
    <Pressable onPress={navigate} style={styles.container}>
      <Text style={styles.text}>{text}</Text>
      <Image
        style={styles.icon}
        source={require('celerity/src/assets/buttons/arrow.png')}
      />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '90%',
    height: 50,
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 15,
    backgroundColor: Colors.white,
    paddingVertical: 15,
    paddingHorizontal: 30,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    width: 15,
    height: 26,
  },
  text: {
    textAlign: 'center',
    fontSize: 18,
    color: Colors.grey,
  },
});

export default MenuButton;
