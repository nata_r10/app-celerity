import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Colors from 'celerity/src/res/colors';
import MenuScreen from 'celerity/src/components/club/MenuScreen';
import PointsScreen from 'celerity/src/components/club/PointsScreen';
import StatusScrenn from 'celerity/src/components/club/StatusScrenn';
import CardScreen from 'celerity/src/components/club/CardScreen';
import RetrieveScreen from 'celerity/src/components/club/RetrieveScreen';

const Stack = createStackNavigator();

const ClubStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.white,
        },
        headerTintColor: Colors.black,
      }}>
      <Stack.Screen name="Club" component={MenuScreen} />
      <Stack.Screen name="Consulta Puntos" component={PointsScreen} />
      <Stack.Screen name="Estado de Cuenta" component={StatusScrenn} />
      <Stack.Screen name="Tarjeta Virtual" component={CardScreen} />
      <Stack.Screen name="Historial de pago" component={RetrieveScreen} />
    </Stack.Navigator>
  );
};

export default ClubStack;
