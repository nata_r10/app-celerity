import React from 'react';
import {SafeAreaView, StyleSheet, View, ScrollView} from 'react-native';
import HeaderLogo from 'celerity/src/components/Header/HeaderLogo';
import MenuButton from '../Buttons/MenuButton';

const MenuScreen = (props) => {
  const navigate = (component) => {
    props.navigation.navigate(component);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <HeaderLogo />
        <View style={styles.buttons}>
          <MenuButton
            text="Consulta Puntos"
            navigate={() => navigate('Consulta Puntos')}
          />
          <MenuButton
            text="Estado de Cuenta"
            navigate={() => navigate('Estado de Cuenta')}
          />
          <MenuButton
            text="Tarjeta Virtual"
            navigate={() => navigate('Tarjeta Virtual')}
          />
          <MenuButton
            text="Historial de pago"
            navigate={() => navigate('Historial de pago')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  buttons: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default MenuScreen;
