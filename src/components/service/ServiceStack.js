import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Colors from 'celerity/src/res/colors';
import MenuScreen from './MenuScreen';

const Stack = createStackNavigator();

const ServiceStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.white,
        },
        headerTintColor: Colors.black,
      }}>
      <Stack.Screen name="Service" component={MenuScreen} />
    </Stack.Navigator>
  );
};

export default ServiceStack;
