import React from 'react';
import {SafeAreaView, StyleSheet, View, ScrollView} from 'react-native';
import HeaderLogo from 'celerity/src/components/Header/HeaderLogo';
import MenuButton from '../Buttons/MenuButton';

const MenuScreen = (props) => {
  const navigate = (url_ext) => {
    props.navigation.navigate('Browser', {url: url_ext});
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <HeaderLogo />
        <View style={styles.buttons}>
          <MenuButton
            text="Planes"
            navigate={() => navigate('https://www.celerity.ec/planes/')}
          />
          <MenuButton
            text="Servicios adicionales"
            navigate={() => navigate('https://www.celerity.ec/planes-guard/')}
          />
          <MenuButton
            text="Tu plan ideal"
            navigate={() =>
              navigate('https://www.celerity.ec/asistente-casa-oficina/')
            }
          />
          <MenuButton
            text="Cobertura"
            navigate={() => navigate('https://www.celerity.ec/cobertura')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  buttons: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default MenuScreen;
