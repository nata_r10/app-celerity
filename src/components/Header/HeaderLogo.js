import React from 'react';
import {
  StyleSheet,
  Image,
  View,
} from 'react-native';


const HeaderLogo = () => {
  return (
    <View style={styles.logocontainer}>
        <Image style={styles.logo} source={require('../../assets/logo-celerity.png')} />
    </View>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: 252,
    height: 70,
  },
  logocontainer: {
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: 20,
  }
});

export default HeaderLogo;
