import React from 'react';
import {SafeAreaView, StyleSheet, View, ScrollView, Text} from 'react-native';
import HeaderLogo from 'celerity/src/components/Header/HeaderLogo';

const ChannelScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <HeaderLogo />
        <View style={styles.buttons}>
          <Text>AccountScreen</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default ChannelScreen;
