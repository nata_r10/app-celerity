import React from 'react';
import {SafeAreaView, StyleSheet, View, ScrollView} from 'react-native';
import HeaderLogo from 'celerity/src/components/Header/HeaderLogo';
import MenuButton from '../Buttons/MenuButton';

const MenuScreen = (props) => {
  const navigate = (component) => {
    props.navigation.navigate(component);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <HeaderLogo />
        <View style={styles.buttons}>
          <MenuButton
            text="Atención al Cliente"
            navigate={() => navigate('Atención al Cliente')}
          />
          <MenuButton
            text="Gestiona tu Cuenta"
            navigate={() => navigate('Gestiona tu Cuenta')}
          />
          <MenuButton
            text="Solicita Traslado"
            navigate={() => navigate('Solicita Traslado')}
          />
          <MenuButton
            text="Aumenta Velocidad"
            navigate={() => navigate('Aumenta Velocidad')}
          />
          <MenuButton
            text="Chat con asesor"
            navigate={() => navigate('Chat con asesor')}
          />
          <MenuButton text="Web Mail" navigate={() => navigate('Web Mail')} />
          <MenuButton
            text="Contáctanos"
            navigate={() => navigate('Contáctanos')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  buttons: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default MenuScreen;
