import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Text, View} from 'react-native';
import Colors from 'celerity/src/res/colors';
import MenuScreen from 'celerity/src/components/atencion/MenuScreen';
import AccountScreen from 'celerity/src/components/atencion/AccountScreen';
import MoveScrenn from 'celerity/src/components/atencion/MoveScrenn';
import IncreaseScreen from 'celerity/src/components/atencion/IncreaseScreen';
import ChatScreen from 'celerity/src/components/atencion/ChatScreen';
import MailScreen from 'celerity/src/components/atencion/MailScreen';
import ContactScreen from 'celerity/src/components/atencion/ContactScreen';

const StackAt = createStackNavigator();

const AtencionStack = () => {
  return (
    <StackAt.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.white,
        },
        headerTintColor: Colors.black,
      }}>
      <StackAt.Screen name="Atención al Cliente" component={MenuScreen} />
      <StackAt.Screen name="Gestiona tu Cuenta" component={AccountScreen} />
      <StackAt.Screen name="Solicita Traslado" component={MoveScrenn} />
      <StackAt.Screen name="Aumenta Velocidad" component={IncreaseScreen} />
      <StackAt.Screen name="Chat con asesor" component={ChatScreen} />
      <StackAt.Screen name="Web Mail" component={MailScreen} />
      <StackAt.Screen name="Contáctanos" component={ContactScreen} />
    </StackAt.Navigator>
  );
};

export default AtencionStack;
