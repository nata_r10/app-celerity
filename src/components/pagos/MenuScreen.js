import React from 'react';
import {SafeAreaView, StyleSheet, View, ScrollView} from 'react-native';
import HeaderLogo from 'celerity/src/components/Header/HeaderLogo';
import MenuButton from '../Buttons/MenuButton';

const MenuScreen = (props) => {
  const navigate = (component) => {
    props.navigation.navigate(component);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <HeaderLogo />
        <View style={styles.buttons}>
          <MenuButton
            text="Paga tu factura en línea"
            navigate={() => navigate('Paga tu factura')}
          />
          <MenuButton
            text="Realiza un convenio de pago"
            navigate={() => navigate('Convenio de Pago')}
          />
          <MenuButton
            text="Revisa tu factura"
            navigate={() => navigate('Revisa tu factura')}
          />
          <MenuButton
            text="Revisa tu historial de pago"
            navigate={() => navigate('Historial de pago')}
          />
          <MenuButton
            text="Conoce otros canales de pago"
            navigate={() => navigate('Canales de pago')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  buttons: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  footer: {
    width: '100%',
    height: 300,
  },
});

export default MenuScreen;
