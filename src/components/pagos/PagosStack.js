import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Colors from 'celerity/src/res/colors';
import PaymentsScreen from 'celerity/src/components/pagos/PaymentsScreen';
import MenuScreen from 'celerity/src/components/pagos/MenuScreen';
import DealScrenn from 'celerity/src/components/pagos/DealScrenn';
import InvoiceScreen from 'celerity/src/components/pagos/InvoiceScreen';
import HistoryScreen from 'celerity/src/components/pagos/HistoryScreen';
import ChannelScreen from 'celerity/src/components/pagos/ChannelScreen';

const Stack = createStackNavigator();

const HomeScreen = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.white,
        },
        headerTintColor: Colors.black,
      }}>
      <Stack.Screen name="Pagos" component={MenuScreen} />
      <Stack.Screen name="Paga tu factura" component={PaymentsScreen} />
      <Stack.Screen name="Convenio de Pago" component={DealScrenn} />
      <Stack.Screen name="Revisa tu factura" component={InvoiceScreen} />
      <Stack.Screen name="Historial de pago" component={HistoryScreen} />
      <Stack.Screen name="Canales de pago" component={ChannelScreen} />
    </Stack.Navigator>
  );
};

export default HomeScreen;
