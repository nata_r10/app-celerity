import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {WebView} from 'react-native-webview';

const BackButton = ({navigation}) => {
  <TouchableOpacity
    onPress={() => {
      navigation.goBack();
    }}>
    <Text>Atrás</Text>
  </TouchableOpacity>;
};

class Browser extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }

  static navigationOptions = ({navigation}) => ({
    title: 'Browser',
    headerLeft: <BackButton navigation={navigation} />,
  });
  render() {
    return (
      <View style={{flex: 1}}>
        <WebView
          source={{
            uri: this.props.route.params.url,
          }}
          style={{flex: 1}}
        />
      </View>
    );
  }
}

export default Browser;
